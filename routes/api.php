<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('deposits/callback', 'DepositsController@update');
Route::post('withdraws/callback', 'WithdrawsController@update');
Route::post('deposits/refresh', 'DepositsController@refresh');
Route::post('withdraws/refresh', 'WithdrawsController@refresh');
