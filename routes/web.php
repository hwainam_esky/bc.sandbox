<?php

use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TestBroadcastEvent implements ShouldBroadcast
{
    public $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function broadcastOn()
    {
        return ['test-channel'];
    }
}

Route::get('/', function () {
    return redirect('/deposits');
});
Route::get('/deposits', 'DepositsController@index')->name('depositsIndex');
Route::get('/deposits/create', 'DepositsController@create');
Route::post('/deposits/create', 'DepositsController@store');

Route::get('/withdraws', 'WithdrawsController@index');
Route::get('/withdraws/create', 'WithdrawsController@create');
Route::post('/withdraws/create', 'WithdrawsController@store');

Route::get('/bridge', function() {
    $pusher = App::make('pusher');

    $pusher->trigger( 'test-channel',
                      'test-event',
                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

    return view('welcome');
});

Route::get('/broadcast', function() {
    event(new TestBroadcastEvent('Broadcasting in Laravel using Pusher!'));

    return view('welcome');
});