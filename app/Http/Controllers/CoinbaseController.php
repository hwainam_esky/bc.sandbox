<?php

namespace App\Http\Controllers;

use App\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Ixudra\Curl\Facades\Curl;

class CoinbaseController extends Controller
{

	public function getPriceIndex() {
		$response = Curl::to('https://api.coinbase.com/v2/prices/BTC-USD/buy')
		->withHeader('CB-VERSION: 2017-09-27')
		->asJson()
		->get();
		return $response->data->amount;
	}

	public function requestDeposit($receiveAmount) {
		$params['path'] = '/v2/orders';
		$params['method'] = 'POST';
		$body['amount'] = number_format($receiveAmount, 6, '.', ',');
		$body['currency'] = 'BTC';
		$body['name'] = 'New Deposit';
		$res = $this->coinabaseRequest($params, $body);
		$json['address'] = $res->data->bitcoin_address;
		$json['amount_btc'] = $res->data->bitcoin_amount->amount;
		$json['state'] = $res->data->status;
		$json['transaction_id'] = $res->data->id;
		$json['wallet'] = 'coinbase';
		return $json;
	}

	// Call this function after requestDeposit callback
	public function sellCoins($receiveAmount) {
		$params['path'] = '/v2/accounts/' . env('COINBASE_BTC_ACCOUNT') . '/sells';
		$params['method'] = 'POST';
		$body['amount'] = number_format($receiveAmount, 6, '.', ',');
		$body['currency'] = 'BTC';
		$res = $this->coinabaseRequest($params, $body);
	}

	public function buyCoins($sendAmount) {
		$params['path'] = '/v2/accounts/' . env('COINBASE_BTC_ACCOUNT') . '/buys';
		$params['method'] = 'POST';
		$body['amount'] = number_format($sendAmount, 6, '.', ',');
		$body['currency'] = 'BTC';
		$res = $this->coinabaseRequest($params, $body);
	}

	public function requestWithdraw($sendAmount) {
		$params['path'] = '/v2/accounts/' . env('COINBASE_BTC_ACCOUNT') . '/transactions';
		$params['method'] = 'POST';
		$body['type'] = 'send';
		$body['to'] = request('address');
		$body['amount'] = number_format($sendAmount, 6, '.', ',');
		$body['currency'] = 'BTC';
		$res = $this->coinabaseRequest($params, $body);
		$json['amount_btc'] = $res->data->amount->amount;
		$json['state'] = $res->data->status;
		$json['transaction_id'] = $res->data->id;
		$json['wallet'] = 'coinbase';
		return $json;
	}

	public function updateDepositStatus($transactionID) {
		$params['path'] = '/v2/orders/' . $transactionID;
		$params['method'] = 'GET';
		$body = array();
		$res = $this->coinabaseRequest($params, $body);
		$json['transaction_id'] = $res->data->id;
		$json['state'] = $res->data->status;
		return $json;
	}

	public function updateWithdrawStatus($transactionID) {
		$params['path'] = '/v2/accounts/' . env('COINBASE_BTC_ACCOUNT') . '/transactions/' . $transactionID;
		$params['method'] = 'GET';
		$body = array();
		$res = $this->coinabaseRequest($params, $body);
		$json['transaction_id'] = $res->data->id;
		$json['state'] = $res->data->status;
		return $json;
	}

	public function update(Request $request) {
		$res = $this->authenticate_callback($request->header('cpt-key'), $request->header('cpt-hmac'), $request->get('data'));

		if ($res['auth']) {
			$body = json_decode(base64_decode($request->get('data')));
			Deposit::where('transaction_id', $body->transaction_id)->update(['state' => $body->state]);

			$pusher = App::make('pusher');
			$pusher->trigger( 'deposit-index',
				'reload-event',
				array('text' => 'Reload Page'));
		}
	}

	public function coinabaseRequest(array $params, array $body) {
		$timestamp = $this->getTimestamp();
		if ($params['method'] == 'POST') {
			$signature = $this->getHash('sha256', $timestamp . $params['method'] . $params['path'] . json_encode($body), env('COINBASE_APP_SECRET'));
			$response = Curl::to('https://api.coinbase.com' . $params['path'])
			->withHeader('CB-ACCESS-KEY: ' . env('COINBASE_APP_KEY'))
			->withHeader('CB-ACCESS-SIGN: ' . $signature)
			->withHeader('CB-ACCESS-TIMESTAMP: ' . $timestamp)
			->withHeader('CB-VERSION: 2017-09-27')
			->withData($body)
			->asJson()
			->post();
		}else {
			$signature = $this->getHash('sha256', $timestamp . $params['method'] . $params['path'], env('COINBASE_APP_SECRET'));
			$response = Curl::to('https://api.coinbase.com' . $params['path'])
			->withHeader('CB-ACCESS-KEY: ' . env('COINBASE_APP_KEY'))
			->withHeader('CB-ACCESS-SIGN: ' . $signature)
			->withHeader('CB-ACCESS-TIMESTAMP: ' . $timestamp)
			->withHeader('CB-VERSION: 2017-09-27')
			->asJson()
			->get();
		}

		return $response;
	}

	public function coinapultRequest(array $params) {
		$b58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		$nonce = '';
		for ($i = 0; $i < 22; $i++) {
			$char = $b58[mt_rand(0, 57)];
			$nonce = $nonce . $char;
		}
		$params['nonce'] = $nonce;
		$params['timestamp'] = (string)time();

		$signdata = base64_encode(json_encode($params));
		$signature  = hash_hmac('sha512', $signdata, env('COINAPULT_APP_SECRET'));
		$client = new \GuzzleHttp\Client([
			'headers' => [ 'cpt-key' => env('COINAPULT_APP_KEY'),
			'cpt-hmac' => $signature]
		]);
		$res = $client->request('POST', 'https://playground.coinapult.com/api'. $params['endpoint'], ['form_params' => [ 'data' => $signdata ]]);
		return $res->getBody();
	}

	public function authenticate_callback($recv_key, $recv_hmac, $recv_data) {
		$res = array();
		$res['auth'] = FALSE;
		$res['hmac'] = '';
		if (!(strcmp($recv_key, env('COINAPULT_APP_KEY')))) {
			/* API key matches. */
			$res['hmac'] = hash_hmac("sha512", $recv_data, env('COINAPULT_APP_SECRET'));
			if (!(strcasecmp($res['hmac'], $recv_hmac))) {
				/* Received HMAC matches. */
				$res['auth'] = TRUE;
			}
		}
		return $res['auth'];
	}

	protected function getTimestamp()
	{
		return time();
	}
	protected function getHash($algo, $data, $key)
	{
		return hash_hmac($algo, $data, $key);
	}
}
