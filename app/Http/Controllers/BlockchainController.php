<?php

namespace App\Http\Controllers;

use App\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Ixudra\Curl\Facades\Curl;

class BlockchainController extends Controller
{

	public function getPriceIndex() {
		$response = Curl::to('https://api.coinbase.com/v2/prices/BTC-USD/buy')
		->withHeader('CB-VERSION: 2017-09-27')
		->asJson()
		->get();
		return $response->data->amount;
	}

	public function requestDeposit($receiveAmount) {
		$root_url = 'https://api.blockchain.info/v2/receive';

		$parameters = 'xpub=' .env('BLOCKCHAIN_XPUB'). '&callback=' .urlencode(env('DEPOSIT_CALLBACK_URL')). '&key=' .env('BLOCKCHAIN_APP_KEY');
		$response = file_get_contents($root_url . '?' . $parameters);
		$res = json_decode($response);
		$json['address'] = $res->address;
		$json['amount_btc'] = $receiveAmount;
		$json['state'] = $res->index;
		$json['transaction_id'] = $res->address;
		$json['wallet'] = 'blockchain';
		return $json;
	}

	public function requestWithdraw($sendAmount) {
		$params['endpoint'] = '/t/send';
		$params['amount'] = number_format($sendAmount, 6, '.', ',');
		$params['currency'] = 'BTC';
		$params['address'] = request('address');
		$params['callback'] = env('WITHDRAW_CALLBACK_URL');
		$res = json_decode($this->coinapultRequest($params));
		$json['amount_btc'] = $res->in->expected;
		$json['state'] = $res->state;
		$json['transaction_id'] = $res->transaction_id;
		$json['wallet'] = 'coinapult';
		return $json;
	}

	public function updateDepositStatus($transactionID) {
		$body['address'] = $transactionID;
		$body['key'] = env('BLOCKCHAIN_APP_KEY');
		$body['onNotification'] = 'KEEP';
		$body['callback'] = env('DEPOSIT_CALLBACK_URL');
		$response = Curl::to('https://api.blockchain.info/v2/receive/balance_update')
		->withHeader('Content-Type: text/plain')
		->withData($body)
		->post();
		var_dump($response);
		$res = json_decode($response);
		$json['transaction_id'] = $res->addr;
		$json['state'] = $res->id;
		return $json;
	}

	public function updateWithdrawStatus($transactionID) {

		// $body['address'] = $transactionID;
		// $body['key'] = env('BLOCKCHAIN_APP_KEY');
		// $body['onNotification'] = 'KEEP';
		// $body['callback'] = env('DEPOSIT_CALLBACK_URL');
		// $response = Curl::to('https://api.blockchain.info/v2/receive/balance_update')
		// 	->withData($body)
		// 	->post();
		// $res = json_decode($response)
		// $json['transaction_id'] = $res->addr;
		// $json['state'] = $res->id;
		// return $json;
	}

	public function update(Request $request) {
		$res = $this->authenticate_callback($request->header('cpt-key'), $request->header('cpt-hmac'), $request->get('data'));

		if ($res['auth']) {
			$body = json_decode(base64_decode($request->get('data')));
			Deposit::where('transaction_id', $body->transaction_id)->update(['state' => $body->state]);

			$pusher = App::make('pusher');
			$pusher->trigger( 'deposit-index',
				'reload-event',
				array('text' => 'Reload Page'));
		}
	}

	public function coinapultRequest(array $params) {
		$b58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		$nonce = '';
		for ($i = 0; $i < 22; $i++) {
			$char = $b58[mt_rand(0, 57)];
			$nonce = $nonce . $char;
		}
		$params['nonce'] = $nonce;
		$params['timestamp'] = (string)time();

		$signdata = base64_encode(json_encode($params));
		$signature  = hash_hmac('sha512', $signdata, env('COINAPULT_APP_SECRET'));
		$client = new \GuzzleHttp\Client([
			'headers' => [ 'cpt-key' => env('COINAPULT_APP_KEY'),
			'cpt-hmac' => $signature]
		]);
		$res = $client->request('POST', 'https://playground.coinapult.com/api'. $params['endpoint'], ['form_params' => [ 'data' => $signdata ]]);
		return $res->getBody();
	}

	public function authenticate_callback($recv_key, $recv_hmac, $recv_data) {
		$res = array();
		$res['auth'] = FALSE;
		$res['hmac'] = '';
		if (!(strcmp($recv_key, env('COINAPULT_APP_KEY')))) {
			/* API key matches. */
			$res['hmac'] = hash_hmac("sha512", $recv_data, env('COINAPULT_APP_SECRET'));
			if (!(strcasecmp($res['hmac'], $recv_hmac))) {
				/* Received HMAC matches. */
				$res['auth'] = TRUE;
			}
		}
		return $res['auth'];
	}
}
