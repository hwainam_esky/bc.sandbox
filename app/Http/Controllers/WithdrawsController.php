<?php

namespace App\Http\Controllers;

use App\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class WithdrawsController extends CoinapultController
{
	public function index() {
		$withdraws = Withdraw::all();

		return view('withdraws.index', compact('withdraws'));
	}

	public function create() {
		$value = $this->getPriceIndex();
		return view('withdraws.create', compact('value'));
	}

	public function store() {
		$response = $this->requestWithdraw(request('btc_amount'));
		Withdraw::create([
			'address' => request('address'),
			'amount_btc' => $response['amount_btc'],
			'amount_hkd' => request('withdraw_amount'),
			'state' => $response['state'],
			'transaction_id' => $response['transaction_id'],
			'wallet' => $response['wallet']
		]);
		$pusher = App::make('pusher');
		$pusher->trigger( 'withdraw-index',
			'reload-event',
			array('text' => 'Reload Page'));
		return redirect('/withdraws');
	}

	public function refresh(Request $request) {
		$response = $this->updateWithdrawStatus($request->get('id'));
		Withdraw::where('transaction_id', $response['transaction_id'])->update(['state' => $response['state']]);
		$pusher = App::make('pusher');
			$pusher->trigger( 'withdraw-index',
				'reload-event',
				array('text' => 'Reload Page'));
	}

	public function update(Request $request) {

		if ($this->authenticate_callback($request->header('cpt-key'), $request->header('cpt-hmac'), $request->get('data'))) {
			$body = json_decode(base64_decode($request->get('data')));
			Withdraw::where('transaction_id', $body->transaction_id)->update(['state' => $body->state]);
			$pusher = App::make('pusher');
			$pusher->trigger( 'withdraw-index',
				'reload-event',
				array('text' => 'Reload Page'));
		}
	}
}
