<?php

namespace App\Http\Controllers;

use App\Deposit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DepositsController extends CoinbaseController
{
	public function index() {
		$deposits = Deposit::all();
		return view('deposits.index', compact('deposits'));
	}

	public function create() {
		$value = $this->getPriceIndex();
		return view('deposits.create', compact('value'));
	}

	public function store() {
		$amount = json_decode(request('amount'));
		$response = $this->requestDeposit($amount->btc);
		Deposit::create([
			'address' => $response['address'],
			'amount_btc' => $response['amount_btc'],
			'amount_hkd' => $amount->hkd,
			'state' => $response['state'],
			'transaction_id' => $response['transaction_id'],
			'wallet' => $response['wallet']
		]);
		$pusher = App::make('pusher');
		$pusher->trigger( 'deposit-index',
			'reload-event',
			array('text' => 'Reload Page'));
		return redirect('/deposits');
	}

	public function refresh(Request $request) {
		$response = $this->updateDepositStatus($request->get('id'));
		Deposit::where('transaction_id', $response['transaction_id'])->update(['state' => $response['state']]);
		$pusher = App::make('pusher');
			$pusher->trigger( 'deposit-index',
				'reload-event',
				array('text' => 'Reload Page'));
	}

	public function update(Request $request) {

		if ($this->authenticate_callback($request->header('cpt-key'), $request->header('cpt-hmac'), $request->get('data'))) {
			$body = json_decode(base64_decode($request->get('data')));
			Deposit::where('transaction_id', $body->transaction_id)->update(['state' => $body->state]);

			$pusher = App::make('pusher');
			$pusher->trigger( 'deposit-index',
				'reload-event',
				array('text' => 'Reload Page'));
		}
	}

}
