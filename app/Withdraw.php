<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = ['address', 'amount_btc', 'amount_hkd', 'user_id', 'state', 'transaction_id', 'wallet'];
}
