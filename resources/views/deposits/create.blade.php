@extends ('layouts.master')

@section ('content')

<h1>New Deposit</h1>

<hr>

<form method="POST" action="/deposits/create">

	{{ csrf_field() }}

	<p>
		<li>10000 HKD = {{ number_format(1300/$value, 6, '.', ',') }} BTC</li>
		<li>20000 HKD = {{ number_format(2600/$value, 6, '.', ',') }} BTC</li>
		<li>50000 HKD = {{ number_format(6500/$value, 6, '.', ',') }} BTC</li>
		<li>100000 HKD = {{ number_format(13000/$value, 6, '.', ',') }} BTC</li>
	</p>

	<div class="form-group">
		<label for="amount">Amount(HKD) :</label>
		<select class="form-control" id="amount" name="amount">
			<option value='{"btc":"{{ number_format(1300/$value, 6, '.', ',') }}","hkd":"10000"}' >10000</option>
			<option value='{"btc":"{{ number_format(2600/$value, 6, '.', ',') }}","hkd":"20000"}'>20000</option>
			<option value='{"btc":"{{ number_format(6500/$value, 6, '.', ',') }}","hkd":"50000"}'>50000</option>
			<option value='{"btc":"{{ number_format(13000/$value, 6, '.', ',') }}","hkd":"100000"}'>100000</option>
		</select>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary">Deposit</button>
	</div>
	@include ('layouts.errors')
</form>

@endsection