@extends ('layouts.master')

@section ('content')
<h1>Deposit</h1>
<br>
<a class="btn btn-primary" href="/deposits/create" role="button">New Deposit</a>
<hr>
<table class="table">
	<thead>
		<tr>
			<th>Transaction ID</th>
			<th>Amount Deposit(HKD)</th>
			<th>Amount To Pay(BTC)</th>
			<th>Status</th>
			<th>QR</th>
			<th>Refresh</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($deposits as $deposit)
		<tr>
			<td scope="row">{{ $deposit->transaction_id }}</td>
			<td>{{ $deposit->amount_hkd }}</td>
			<td>{{ $deposit->amount_btc }}</td>
			<td>{{ $deposit->state}}</td>
			<td>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#qrModal" data-address="{{ $deposit->address }}" data-qr="{!! base64_encode(QrCode::format('png')->size(400)->BTC($deposit->address, $deposit->amount_btc)); !!}">show</button>
			</td>
			<td>
				<button type="button" class="refreshTrans btn btn-primary" data-target="#refreshTrans" data-transaction="{{ $deposit->transaction_id }}" >Refresh</button>
			</td>
		</tr>

		@endforeach
	</tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="qrModal" tabindex="-1" role="dialog" aria-labelledby="qrModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel"></h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="visible-print text-center">
					<img id="qrImage" src="">
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section ('js')
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>
	var pusher = new Pusher("{{ env("PUSHER_APP_KEY") }}", {cluster: 'ap1'});
	var channel = pusher.subscribe('deposit-index');
	channel.bind('reload-event', function(data) {
		document.location.reload(true);
	});

	$('#qrModal').on('show.bs.modal', function (event) {
  	var button = $(event.relatedTarget) // Button that triggered the modal
  	var transaction = button.data('address') // Extract info from data-* attributes
  	var qr = button.data('qr')

  	var modal = $(this)
  	modal.find('.modal-title').text(transaction)
  	modal.find('#qrImage').attr("src","data:image/png;base64, " + qr)
  });
	$('.refreshTrans').click(function() {
		var transaction = $(this).data("transaction");
		$.ajax({
			method: "POST",
			url: './api/deposits/refresh',
			data: { id : transaction },
			success: function() {
				console.log("Updated status");
			}
		})
	});

</script>
@endsection