@extends ('layouts.master')

@section ('content')

<h1>New Withdraw</h1>

<hr>

<form method="POST" action="/withdraws/create">

	{{ csrf_field() }}

	<div class="form-group">
		<label for="address">Bitcoin Address :</label>
		<input type="text" class="form-control" id="address" name="address">
	</div>

	<div class="form-group">
		<label for="withdraw_amount">Withdraw Amount(HKD) :</label>
		<input type="number" class="form-control" id="withdraw_amount" min=0 name="withdraw_amount" required="true">
	</div>

	<div class="form-group">
		<label for="btc_amount">Bitcoin :</label>
		<input type="number" class="form-control" id="btc_amount" min=0 name="btc_amount" required="true" value=0.000000 readonly>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary">Withdraw</button>
	</div>
	@include ('layouts.errors')
</form>
@endsection

@section ('js')
<script>
	$('#withdraw_amount').keyup(function(){
		var amount = $(this).val() / 8 /{{ $value}} ;
		$('#btc_amount').val(amount.toFixed(6));
	});
</script>
@endsection