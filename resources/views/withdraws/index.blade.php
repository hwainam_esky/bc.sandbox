@extends ('layouts.master')

@section ('content')
<h1>Withdraw</h1>
<br>
<a class="btn btn-primary" href="/withdraws/create" role="button">New Withdraw</a>
<hr>
<table class="table">
	<thead>
		<tr>
			<th>Transaction ID</th>
			<th>Amount Deposit(HKD)</th>
			<th>Amount To Pay(BTC)</th>
			<th>Status</th>
			<th>Refresh</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($withdraws as $withdraw)
		<tr>
			<td scope="row">{{ $withdraw->transaction_id }}</td>
			<td>{{ $withdraw->amount_hkd }}</td>
			<td>{{ $withdraw->amount_btc }}</td>
			<td>{{ $withdraw->state}}</td>
			<td>
				<button type="button" class="refreshTrans btn btn-primary" data-target="#refreshTrans" data-transaction="{{ $withdraw->transaction_id }}" >Refresh</button>
			</td>
		</tr>

		@endforeach
	</tbody>
</table>
@endsection

@section ('js')
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script>
	var pusher = new Pusher("{{ env("PUSHER_APP_KEY") }}", {cluster: 'ap1'});
	var channel = pusher.subscribe('withdraw-index');
	channel.bind('reload-event', function(data) {
		document.location.reload(true);
	});
	$('.refreshTrans').click(function() {
		var transaction = $(this).data("transaction");
		$.ajax({
			method: "POST",
			url: './api/withdraws/refresh',
			data: { id : transaction },
			success: function() {
				console.log("Updated status");
			}
		})
	});
</script>
@endsection